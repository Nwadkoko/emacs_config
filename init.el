(require 'org)
(org-babel-load-file (expand-file-name (concat user-emacs-directory "settings.org")))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (auctex web-mode flycheck json-mode go-mode yaml-mode js3-mode php-auto-yasnippets flymake-php modern-cpp-font-lock clang-format eglot material-theme company-lsp ccls lsp-ui lsp-mode hungry-delete rainbow-delimiters magit avy which-key try ivy spaceline use-package)))
 '(projectile-project-root-files-top-down-recurring
   (quote
    ("compile_commands.json" ".ccls" "compile_commands.json" ".ccls" "compile_commands.json" ".ccls" "compile_commands.json" ".ccls" "compile_commands.json" ".ccls" "compile_commands.json" ".ccls" "compile_commands.json" ".ccls" ".svn" "CVS" "Makefile"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


