(require 'package)
 
    (setq package-enable-at-startup nil)
    (setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
             ("marmalade" . "http://marmalade-repo.org/packages/")
             ("melpa" . "http://melpa.milkbox.net/packages/")))

    (package-initialize)

    (unless (package-installed-p 'use-package)
      (package-refresh-contents)
      (package-install 'use-package))

(tool-bar-mode -1)
(menu-bar-mode -1)

(scroll-bar-mode -1)

(set-face-attribute 'default nil :height 175)

(add-to-list 'default-frame-alist '(fullscreen . maximized))

(setq electric-pair-pairs
        '(
          (?" . ?")
          (?{ . ?})))

        (electric-pair-mode)

(global-auto-revert-mode t)

(setq inhibit-startup-message t)
(setq ring-bell-function 'ignore)

(display-line-numbers-mode 1)

(use-package try
:ensure t)

(use-package which-key
:ensure t
:config (which-key-mode))

(use-package swiper
:ensure try
  :bind (("C-f" . swiper)
     ("C-F" . swiper)
     ("C-c C-r" . ivy-resume)
     ("M-x" . counsel-M-x)
     ("C-x C-f" . counsel-find-file))
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq ivy-display-style 'fancy)
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
    ))

(use-package avy
  :ensure t
  :bind ("M-f" . avy-goto-word-1))

(use-package magit
:ensure t
:init
(bind-key "C-c g" 'magit-status))

(use-package rainbow-delimiters
 :ensure t
 :config 
 (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package hungry-delete
     :ensure t
     :config
     (global-hungry-delete-mode))

(use-package spaceline
 
   :ensure t
    :init
    (setq powerline-default-separator 'slant)
    :config
    (spaceline-emacs-theme)
    (spaceline-toggle-minor-modes-off)
    (spaceline-toggle-buffer-size-off)
    (spaceline-toggle-evil-state-on))

(use-package dracula-theme
  :ensure t
  :config
  (load-theme 'dracula t))

(setq indo-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)

(defalias 'list-buffers 'ibuffer)
(use-package counsel
  :ensure t)

(use-package ivy
  :ensure t
  :diminish (ivy-mode)
  :bind (("C-x b" . ivy-switch-buffer))

  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-display-style 'fancy))

(use-package flycheck
:ensure t
:init (global-flycheck-mode))

(setq org-src-window-setup 'current-window)

(use-package org-bullets
  :ensure t
  :init
  (add-hook 'org-mode-hook (lambda ()
                 (org-bullets-mode 1))))

(setq org-hide-emphasis-markers t)
(font-lock-add-keywords 'org-mode
            '(("^ +\([-*]\) "
               (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

(defun config-reload()
  (interactive)
  (org-babel-load-file (expand-file-name "~/.emacs.d/settings.org")))
(global-set-key (kbd "<f5>") 'config-reload)

(use-package lsp-mode 
  :ensure t
  :commands lsp
  :hook (prog-mode . lsp))

(use-package lsp-ui
  :ensure t
  :commands lsp-ui)

(use-package company-lsp 
  :ensure t
  :commands company-lsp
  :config (push 'company-lsp company-backends))(use-package lsp-mode :commands lsp)
(use-package lsp-ui :commands lsp-ui-mode)

(use-package ccls
     :ensure t
     :after projectile
     :init
     (setq ccls-executable "/home/matthieu/Documents/ccls/Release/ccls")
     (setq ccls-sem-highlight-method 'font-lock)
     (with-eval-after-load 'projectile
	 (add-to-list 'projectile-globally-ignored-directories ".ccls-cache"))
     (setq lsp-prefer-flymake nil)
     (setq-default flycheck-disabled-checkers '(c/c++-clang c/c++-cppcheck c/c++-gcc))
     :custom (projectile-project-root-files-top-down-recurring
		(append '("compile_commands.json" ".ccls")
			projectile-project-root-files-top-down-recurring))
     :hook ((c-mode c++-mode objc-mode) .
	      (lambda () (require 'ccls) (lsp))))

(use-package company
  :diminish ""
  :ensure t
  :config
  (setq company-idle-delay 0.3)
  (setq company-minimum-prefix-length 1)
  (global-company-mode t))

(with-eval-after-load 'company
  (define-key company-active-map (kbd "M-n") nil)
  (define-key company-active-map (kbd "M-p") nil)
  (define-key company-active-map (kbd "H-i") 'company-select-previous)
  (define-key company-active-map (kbd "C-k") 'company-select-next))
   
  (setq company-transformers nil company-lsp-async t company-lsp-cache-candidates nil)

(use-package clang-format
  :ensure t
  :commands clang-format clang-format-buffer clang-format-region)

  (fset 'c++-indent-region 'clang-format-region)

(use-package modern-cpp-font-lock
  :ensure t
  :hook (c++-mode . modern-c++-font-lock-mode))
(add-hook 'lua-mode-hook 
	      (lambda () (unless (fboundp 'lua-calculate-indentation-right-shift-next)
			   (load-file (locate-file "my-lua.el" load-path)))))

(projectile-global-mode)
(setq projectile-indexing-method 'git)
(setq projectile-enable-cachin t)
(custom-set-faces)

(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(require 'twig-mode)
(add-to-list 'auto-mode-alist '("\\.html\\.twig'" . twig-mode))

(require 'flymake-php)
(add-hook 'php-mode-hook 'flymake-php-load)
(setq flymake-run-in-place nil)
(setq temporary-file-directory "~/.emacs.d/tmp")
(setq flymake-no-changes-timeout 10)
(setq flymake-start-syntax-check-on-newline nil)

(require 'php-auto-yasnippets)
(define-key php-mode-map (kbd "C-c C-y") 'yas/create-php-snippet)

(require 'js3-mode)
(add-to-list 'auto-mode-alist '("\\.js\\'" . js3-mode))

(require 'json-mode)
(add-to-list 'auto-mode-alist '("\\.json\\'" . json-mode))

(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))

(add-to-list 'auto-mode-alist '("\\.css\\'" . css-mode))
(add-to-list 'auto-mode-alist '("\\.scss\\'" . css-mode))
